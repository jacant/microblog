# Flask Mega Tutorial

(Link)[https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world]

## Required Installs (so far)
1. flask
2. flask-wtf
3. flask-sqlalchemy
4. flask-migrate
5. flask-login

## Potential Installs
1. flask-shell-ipython - allows using ipython for interactive exploration of
   your webapp instread of the standard python interpreter, which is used by the
   `flask shell` command.
