#! /usr/bin/env python3

from app import app, db
from app.models import User, Post

# think this is for auto importing database models
# so one can play with them in the python shell
@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}
